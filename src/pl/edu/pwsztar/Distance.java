package pl.edu.pwsztar;

public class Distance {
    private Planet planet1, planet2;
    double distance;

    public Distance(Planet planet1, Planet planet2){
        this.planet1 = planet1;
        this.planet2 = planet2;
    }

    public void calculate(){
        this.distance = Math.sqrt(
                Math.pow((planet1.getPos_x()-planet2.getPos_x()),2)
                + Math.pow((planet1.getPos_y()-planet2.getPos_y()),2)
                + Math.pow((planet1.getPos_z()-planet2.getPos_z()),2)
        );
        System.out.println("Distance between "+planet1.getName()+" and "+planet2.getName()+" is "+distance);
    }

}
