package pl.edu.pwsztar;

public class Planet {
    private double pos_x, pos_y, pos_z;
    private String name;

    public double getPos_x() {
        return pos_x;
    }

    public double getPos_y() {
        return pos_y;
    }

    public double getPos_z() {
        return pos_z;
    }

    public String getName() {
        return name;
    }

    public Planet(double x, double y, double z, String name){
        this.pos_x = x;
        this.pos_y = y;
        this.pos_z = z;
        this.name = name;
    }

}
