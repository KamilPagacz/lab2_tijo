package pl.edu.pwsztar;

public class PasswordChecker {
    private String password;

    public PasswordChecker(String password) {
        this.password = password;
    }

    public void changePassword(final String password) {
        this.password = password;
    }

    public void deletePassword() {
        this.password = null;
    }

    public boolean checkPassword(final String password) {
        return this.password != null && this.password.equals(password);
    }

}
