package pl.edu.pwsztar;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private PasswordChecker password;

    public PasswordChecker getPass(){return password;}

    public User(String firstName, String lastName, int age, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.password = new PasswordChecker(password);
    }
}
