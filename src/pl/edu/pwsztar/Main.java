package pl.edu.pwsztar;

public class Main {

    public static void main(String[] args) {
        User bruceLee = new User("Bruce", "Lee", 24, "karate");
        bruceLee.getPass().changePassword("kungfu");
        System.out.println(bruceLee.getPass().checkPassword("jujitsu")?"true":"false");
        System.out.println(bruceLee.getPass().checkPassword("kungfu")?"true":"false");


        //odległość
        Planet sun = new Planet(0,0,0,"Sun");
        Planet earth = new Planet(146.5,230.2,200.3,"Earth");
        Distance distance = new Distance(sun, earth);
        distance.calculate();


    }
}
